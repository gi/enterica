# S. enterica

2,964 genomes from _Salmonella enterica_ subspecies enterica.

Published in / cite:

- Zhemin Zhou, Inge Lundstrøm, Alicia Tran-Dien, Sebastián Duchêne, Nabil-Fareed Alikhan, Martin J. Sergeant, Gemma Langridge, Anna K. Fotakis, Satheesh Nair, Hans K. Stenøien, Stian S. Hamre, Sherwood Casjens, Axel Christophersen, Christopher Quince, Nicholas R. Thomson, François-Xavier Weill, Simon Y.W. Ho, M. Thomas P. Gilbert, Mark Achtman. "Pan-genome Analysis of Ancient and Modern _Salmonella enterica_ Demonstrates Genomic Stability of the Invasive Para C Lineage for Millennia" Current Biology. 2018. 28(15): 2420-2428.e10.

Figure 2(A) of the above publication shows a phylogeny based on 2,964 assemblies. The assmeblies have been published on Enterobase (http://enterobase.warwick.ac.uk/).

Not all of these assemblies are available on EnteroBase anymore and bulk download from command line is currently not possible.

Here, we provide a snapshot of the published data from 02/2019 for reasons of reproducibility of experiments that have been performed on that data meanwhile, e.g. in

- Roland Wittler "Alignment- and reference-free phylogenomics with colored de Bruijn graphs" Algorithms for Molecular Biology. 2020. 15: 4.
- Andreas Rempel, Roland Wittler "SANS serif: alignment-free, whole-genome based phylogenetic reconstruction" Bioinformatics. 2021.

For completeness, we also provide the phylogenetic trees mentioned in Figure 2(A) of the above manuscript, originally provided here:

- Supertree 3 (RAxML maximum likelihood phylogeny based on a 2.8 Mbp concatenate of 3,002 core genes): http://wrap.warwick.ac.uk/101837/
- Supertree 2 (species tree (ASTRID) based on 3,002 core genes): http://wrap.warwick.ac.uk/101836/

The file list `list.txt` can e.g. be used as input for [SANS](https://gitlab.ub.uni-bielefeld.de/gi/sans) after decompressing the fasta files.

This provision of the data has been permitted by the EnteroBase development team. We thank the team for their valuable help.

When using this data, cite the original publication:

- Zhemin Zhou, Inge Lundstrøm, Alicia Tran-Dien, Sebastián Duchêne, Nabil-Fareed Alikhan, Martin J. Sergeant, Gemma Langridge, Anna K. Fotakis, Satheesh Nair, Hans K. Stenøien, Stian S. Hamre, Sherwood Casjens, Axel Christophersen, Christopher Quince, Nicholas R. Thomson, François-Xavier Weill, Simon Y.W. Ho, M. Thomas P. Gilbert, Mark Achtman. "Pan-genome Analysis of Ancient and Modern _Salmonella enterica_ Demonstrates Genomic Stability of the Invasive Para C Lineage for Millennia" Current Biology. 2018. 28(15): 2420-2428.e10.
